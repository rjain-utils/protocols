(defpackage :rjain.protocols
  (:use :cl)
  (:nicknames :protocols)
  (:export
   ;; protocol.lisp
   #:protocol
   #:protocol-name
   #:protocol-direct-superprotocols
   #:protocol-precedence-list
   #:compute-protocol-precedence-list
   #:protocol-direct-subprotocols
   #:add-direct-subprotocol
   #:remove-direct-subprotocol

   ;; defprotocol.lisp
   #:find-protocol
   #:defprotocol
   #:ensure-protocol
   #:ensure-protocol-using-class
   
   ;; signature.lisp
   #:signature-class
   #:signature
   #:generic-function-signatures
   #:signature-generic-function
   #:signature-specializers
   #:cannonicalize-specializer
   #:add-signature
   #:remove-signature
   #:verify-signature
   #:verify-signature-using-gf
   #:satisfies-signature-p
   #:satisfies-specializer-p

   ;; defsignature.lisp
   #:defsignature
   ))
