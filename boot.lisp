(in-package :rjain.protocols)

;;; Dummy the metacircularly used forms and classes for bootstrapping if they're not already
;;; defined. Since protocols do not provide any extra functionality to the object system, we can get
;;; away with doing nothing. :)

(eval-when (:compile-toplevel :load-toplevel :execute)
  (let ((definer-accessors '((defun . symbol-function)
                             (defmacro . symbol-function)
                             (defclass . (lambda (name)
                                           (find-class name nil))))))
    (defmacro maybe-define ((definer name arglist
                              &body definer-body)
                            &body body)
      `(when (null (,(cdr (assoc definer definer-accessors)) ',name))
         (,definer ,name ,arglist
           ,@definer-body)
         ,@body))))
  

(maybe-define
 (defmacro defprotocol (&rest blah)
   ))

(maybe-define
 (defmacro defsignature (&rest blah)
   ))

(maybe-define
 (defclass protocol-implementing-class (mop::standard-class)
   ())
 (defmethod shared-initialize ((class protocol-implementing-class) slots
                               &key ((implements implements) nil)
                               &rest initargs)
   (call-next-method)))
