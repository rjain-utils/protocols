(in-package :protocols)

;;;;
;;;; The protocol Signature
;;;; 
;;;; Signatures are a declaration of a set of protocols that a generic-function must have methods
;;;; for.
;;;;

(defgeneric signature-class (generic-function)
  (:documentation
   "_Arguments_
    generic-function is the generic function to which the new signature will belong

    _Values_
    The value returned is a class metaobject.

    _Purpose_

    This function is called to determine the class of signatures belonging to some generic
function. The result must be a class that implements the protocol ~(protocol signature)."))


(defprotocol signature ()
  (:documentation
   "All signature metaobjects must be instances of a class that implements the protocol ~(protocol signature).

   Signatures indicate what methods must be implemented for some generic function when classes claim
to implement some protocol.

    The cannonical implementation of the protocol ~(protocol signature) is the class ~(class standard-signature)."))


(defgeneric generic-function-signatures (generic-function)
  (:documentation
   "_Reader for generic function metaobjects_

    Returns the set of signatures currently belonging to ~(arg generic-function). This is a set of
objects implementing the signature protocol. This value is maintained by the generic functions
add-signature and remove-signature."))

(defsignature generic-function-signatures (generic-function))


(defgeneric signature-generic-function (signature)
  (:documentation
   "_Reader for signature metaobjects_

    Returns the generic function to which the ~(arg signature) belongs."))

(defsignature signature-generic-function (signature))


(defgeneric signature-specializers (signature)
  (:documentation
   "_Reader for signature metaobjects_

    Returns a list of the specializers for ~(arg signature)."))

(defsignature signature-specializers (signature))


(defgeneric cannonicalize-specializer (signature specializer)
  (:documentation
   "_Arguments_
    signature is a signature metaobject
    specializer is an object

    _Values_
    The value of this generic function is a specializer object

    _Purpose_

    This generic function is called to convert an object which is passed as one of the specializers
for a signature to whatever object should be used when manipulating the signature itself. The ~(arg
specializer) could be an expression appearing in a ~(fun defsignature) form or an object passed as
an initarg or a member of one of the initargs when constructing a ~(protocol signature)."))

(defsignature cannonicalize-specializer (signature t))


(defgeneric add-signature (generic-function signature)
  (:documentation
   "_Arguments_
    generic-function is a generic function metaobject
    signature is a signature metaobject

    _Values_
    The ~(arg generic-function) argument is returned

    _Purpose_

    This generic function associates a unattached signature with a generic function.

    An error is signalled if the specializer list of ~(arg signature) is not congruent with the
lambda list of ~(arg generic-function). An error is also signalled if ~(arg signature) is already
associated with some other generic function.

    Associating ~(arg signature) with ~(arg generic-function) occurs in three steps: (i) add ~(arg
signature) to the set returned by ~(fun generic-function-signatures) and arrange for ~(fun
signature-generic-function) to return ~(arg generic-function); (ii) call ~(fun add-direct-signature)
for each of ~(arg signature)'s specializers; and (iii) update the dependents of ~(arg generic-function).

    The generic function ~(fun add-signature) may be called by the user or the implementation."))

(defsignature add-signature (generic-function signature))


(defgeneric remove-signature (generic-function signature)
  (:documentation
   "_Arguments_
    generic-function is a generic function metaobject
    signature is a signature metaobject

    _Values_
    The ~(arg generic-function) argument is returned

    _Purpose_

    This generic function breaks the association between a generic function and one of its
signatures.

    No error is signalled if ~(arg signature) is not among the signatures of ~(arg generic-function).

    Breaking the association between ~(arg signature) and ~(arg generic-function) proceeds in three
steps: (i) remove ~(arg signature) from the set returned by ~(fun generic-function-signatures) and
arrange for ~(fun signature-generic-function) to return ~(var nil); (ii) call ~(fun
remove-direct-signature) for each of ~(arg signature)'s specializers; and (iii) update the
dependents of ~(arg generic-function).

    The generic function ~(fun remove-signature) may be called by the user or the implementation."))

(defsignature remove-signature (generic-function signature))


(defun verify-signature (signature)
  (verify-signature-using-gf (signature-generic-function signature) signature))


(defgeneric verify-signature-using-gf (generic-function signature)
  (:documentation
   "_Arguments_
    generic-function is a generic function metaobject
    signature is a signature metaobject

    _Values_
    "))

(defsignature verify-signature-using-gf (generic-function signature))


(defgeneric satisfies-signature-p (signature method))

(defsignature satisfies-signature-p (signature method))


(defgeneric satisfies-specializer-p (signature method sig-spec meth-spec)
  (:method-combination or))

(defsignature satisfies-specializer-p (signature method t t)
  boolean)

