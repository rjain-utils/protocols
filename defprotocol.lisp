(in-package :protocols)

;;;
;;; The function Find-Protocol
;;;

(defvar *protocol-table* (make-hash-table :test 'eq))

(defun find-protocol (name &optional (errorp t))
  (declare (type symbol name))
  (let ((res (gethash name *protocol-table*)))
    (if (or res (not errorp))
        res
      (error "Protocol not yet defined:~%  ~S" name))))

(defun (setf find-protocol) (new-value name &optional (errorp t))
  (declare (type (or null class) new-value)
           (ignore errorp))
  (if (null new-value)
      (remhash name *protocol-table*)
    (setf (gethash name *protocol-table*) new-value)))

;;;
;;; Documentation Accessor
;;;

(defmethod (setf documentation) (new-value (symbol symbol) (type (eql 'protocol)))
  (setf (documentation (find-protocol symbol t) t)
        new-value))
(defmethod documentation ((symbol symbol) (type (eql 'protocol)))
  (documentation (find-protocol symbol t) t))


;;;
;;; The macro DefProtocol
;;;

(defun cannonicalize-direct-superprotocols (direct-superprotocols)
  (mapcar #'cannonicalize-direct-superprotocol direct-superprotocols))

(defun cannonicalize-direct-superprotocol (spec)
  (if (protocolp spec)
      spec
    (find-protocol spec)))

(defun cannonicalize-defprotocol-options (options)
  (mapappend 'cannonicalize-defprotocol-option options))

(defun cannonicalize-defprotocol-option (option)
  (case (first option)
    (:documentation
     `(',(first option) ',(second option)))
    (otherwise
     `(',(first option) ',(rest option)))))

(defmacro defprotocol (name direct-superprotocols &rest options)
  ""
  `(ensure-protocol ',name
                    :direct-superprotocols ,direct-superprotocols
                    ,@(cannonicalize-defprotocol-options options)))

(defun ensure-protocol (name &rest keys &key &allow-other-keys)
  ""
  (apply #'ensure-protocol-using-class (find-protocol name nil) name keys))

(defgeneric ensure-protocol-using-class (protocol name &key &allow-other-keys)
  (:documentation
   ""))

(defmethod ensure-protocol-using-class ((protocol null) name
                                        &rest keys
                                        &key (metaclass 'standard-protocol) (direct-superprotocols )
                                        &allow-other-keys)
  (setf (find-protocol name) (apply #'make-instance metaclass :name name
                                    initargs)))

(defmethod ensure-protocol-using-class ((protocol standard-protocol) name
                                        &rest keys
                                        &key (metaclass 'standard-protocol) (direct-superprotocols )
                                        &allow-other-keys)
  (remf keys :metaclass)
  (setf direct-superprotocols (cannonicalize-direct-superprotocols direct-superprotocols))
  (apply #'change-class protocol metaclass :name name
         initargs))

(eval-when (:load-toplevel :eval-toplevel)
  (ensure-protocol t))

