
(defgeneric generic-function-signatures (generic-function))

(defclass standard-signature ()
  ((generic-function :reader signature-generic-function
                     :initform nil
                     :type (or null generic-function))
   (specializers :initarg :specializers
                 :reader signature-specializers
                 :type list))
  (:metaclass protocol-implementing-class)
  (implements signature))

(defmethod shared-initialize ((signature standard-signature) slots &key specializers)
  (setf (signature-specializers signature)
        (mapcar (lambda (specializer)
                  (cannonicalize-specializer signature specializer))
                specializers)))

(defmethod cannonicalize-specializer ((signature standard-signature) (specializer protocol))
  specializer)

(defmethod cannonicalize-specializer ((signature standard-signature) (specializer class))
  specializer)

(defmethod cannonicalize-specializer ((signature standard-signature) (specializer eql-specializer))
  specializer)

(defmethod cannonicalize-specializer ((signature standard-signature) (specializer symbol))
  (or (find-protocol specializer nil)
      (find-class specializer nil)
      (error "Unable to find a protocol or class named ~S" specializer)))

(defmethod cannonicalize-specializer ((signature standard-signature) (specializer cons))
  (if (and (eq (first specializer) 'eql)
           (endp (nthcdr 2 specializer)))
      (cannonicalize-specializer (intern-eql-specializer (second specializer)))
    (call-next-method)))

(defmethod add-signature ((generic-function standard-generic-function)
                          (signature standard-signature))
  (if (signature-generic-function signature)
      (error "The signature ~S is already associated with a GENERIC-FUNCTION."
             signature)
    (progn
      ;;; TODO: remove any existing signature
      (push signature (generic-function-signatures generic-function))
      (dolist (specializer (signature-specializers signature))
        (add-direct-signature specializer signature))
      (update-dependents generic-function
                         'add-signature signature)
      generic-function)))

(defmethod remove-signature ((generic-function standard-generic-function)
                             (signature standard-signature))
  (when (signature-generic-function signature)
    (deletef signature (generic-function-signatures generic-function))
    (dolist (specializer (signature-specializers signature))
      (remove-direct-signature specializer signature))
    (update-dependents generic-function
                       'remove-signature signature))
  generic-function)

(defmethod verify-signature-using-gf ((generic-function generic-function)
                                      (signature standard-signature))
  ;; !!!
  ;; How do I find out which specializer patterns are required (as specified by CLHS
  ;; define-method-combination long-form-option :required)?? AMOP has no spec on readers for
  ;; method-combinations... :\
  ;; !!!
  (let ((required-methods
         (remove-if (lambda (method)
                      (keywordp (first (method-qualifiers method))))
                    (generic-function-methods generic-function))))
    ;;; XXX Hmm... this flow doesn't strike me as correct...
    (some (lambda (method) (satisfies-signature signature method))
          required-methods)))

(defmethod satisfies-signature-p ((signature standard-signature) (method method))
  (every (lambda (sig-spec meth-spec)
           (satisfies-specializer signature method sig-spec meth-spec))
         (signature-specializers signature)
         (method-specializers method)))

#.`(progn
     ,@(mapcar (lambda (decl)
                 (destructuring-bind ((sig-spec-class meth-spec-class)
                                      &body body)
                     decl
                   `(defmethod satisfies-specializer-p or ((signature standard-signature) (method method) (sig-spec ,sig-spec-class) (meth-spec ,meth-spec-class))
                      ,@body)))
               '(((t t)
                  nil)
                 ((eql-specializer eql-specializer)
                  (eq sig-spec meth-spec))
                 ((class class)
                  (find sig-spec (class-precedence-list meth-spec)))
                 ((standard-protocol class)
                  (find sig-spec (class-effective-protocols meth-spec))))))

