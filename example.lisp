(defprotocol foo ()
  (:documentation "A sample protocol, its only superprotocol is T, the root of all protocols."))

(defgeneric bar (foo x y)
  (:documentation "This is a generic function!"))

(defsignature bar (foo t t)
  (:documentation "The generic function BAR operates on an arglist containing 3 arguments which
implement protocols FOO, T, and T, respectively. "))

(defclass fooz ()
  ()
  (:implements foo)
  (:documentation "A sample class, which will show us how to use protocols.")
  (:metaclass protocol-implementing-class))

(defmethod bar ((foo fooz) x y)
  "If this definition is not present, the class FOOZ does not actually implement the protocol FOO,
and CHECK-PROTOCOLS will indicate this fact.")

(defprotocol arf ()
  (:documentation "Another sample protocol"))

(defgeneric arfa (arf foo))

(defsignature arfa (arf foo)
  (:documentation "A GENERIC-FUNCTION which specifies a pair of protocols for its
arguments. Note that at this point, FOOZ no longer fully implements the FOO protocol."))

(defclass arfie ()
  ()
  (:implements arf)
  (:documentation "Nothing new here")
  (:metaclass protocol-implementing-class))

(defmethod arfa ((arf arfie) (foo fooz))
  "The method which gives ARFIE and FOOZ full implementations of ARF and FOO, respectively.")

(defprotocol baz (foo)
  (:documentation "Protocols can inherit from each other, too. The protocol BAZ now inherits all the PROTOCOL-GENERIC-FUNCTIONs of FOO.")
  (:metaclass protocol-implementing-class))

(defgeneric arfa+ (arf baz))
(defsignature arfa+ (arf baz)
  (:documentation "This GENERIC-FUNCTION is part of the BAZ protocol, but not the FOO
protocol."))

(defclass baas ()
  ()
  (:implements baz)
  (:documentation "BAAS indirectly implements FOO, too")
  (:metaclass protocol-implementing-class))

(defmethod bar ((foo baas))
  "BAAS doesn't inherit from a class which implements FOO, so we'll need to provide that
implementation ourselves.")

(defmethod arfa ((arf arfie) (foo baas))
  "And we'll need to implement this PROTOCOL-GENERIC-FUNCTION for BAAS, too.")

(defmethod arfa+ ((arf arfie) (baz baas))
  "Finally, here is the new PROTOCOL-GENERIC-FUNCTION that BAAS must implement to fully conform to
the BAZ protocol.")


;;; In unit/regression tests, do the following to ensure that the protocols the classes claim to
;;; implement are fully implemented:

(check-protocols :class 'fooz 'arfie 'baas)
