(in-package :protocols)

(defun mapappend (fun &rest args)
   (if (some 'null args)
       '()
       (append (apply fun (mapcar 'car args))
               (mapappend fun (mapcar 'cdr args)))))

(defmacro deletef (item seq-place &key &rest keys &environment env)
  (multiple-value-bind (dummies vals new setter getter)
      (get-setf-expansion seq-place env)
    `(let* (,@(mapcar #'list dummies vals) (,(car new) ,getter))
       (if (cdr new) (error "Can't expand this."))
       (prog1 (setq ,(car new) (delete ,item ,(car new) ,@keys))
         ,setter))))

(defun update-dependents (metaobject &rest initargs)
  (map-dependents metaobject
                  (lambda (dep)
                    (apply 'update-dependent metaobject dep
                           initargs))))
