(in-package :protocols)


(defgeneric class-direct-protocols (class))
(defgeneric class-effective-protocols (class))
(defgeneric add-direct-protocol (class protocol))
(defgeneric remove-direct-protocol (class protocol))
(defgeneric compute-effective-protocols (class))


;;;;
;;;; Hackish implementation of these generic-functions for all classes.
;;;;

(defvar *class-protocol-table* (make-hash-table :test 'eq)
  "Class -> (Direct-protocols . Effective-protocols)")

(defmethod class-direct-protocols ((class class))
  (car (gethash class *class-protocol-table*)))

(defmethod class-effective-protocols ((class class))
  (cdr (gethash class *class-protocol-table*)))

(defmethod add-direct-protocol ((class class) protocol)
  (let ((cons (gethash class *class-protocol-table*)))
    (if cons
        (pushnew class (car cons))
      (setf (gethash class *class-protocol-table*)
            `((,class) . ())))))

(defmethod compute-effective-protocols ((class class))
  (apply 'union (mapcar 'class-effective-protocols
                        (class-direct-superclasses class))))


;;;
;;; The class metaclass Protocol-Implementing-Class
;;;
;;; Main user-visible effect is ability to declare the protocols implemented by this class in the
;;; defclass form, but the implementation is much cleaner, as we can actually store the information
;;; in the class itself. In theory, this will allow the GC to reclaim the class when it is no longer
;;; accessible, but it's always accessible via the direct-superclasses' direct-subclasses slot.
;;;

(defclass protocol-implementing-class (standard-class)
  ((direct-protocols :initarg :direct-protocols
                     :reader class-direct-protocols
                     :type list
                     :initform nil)
   (effective-protocols :reader class-effective-protocols
                        :type list)))

(defmethod shared-initialize ((class protocol-implementing-class) slots
                              &key ((implements implements) nil)
                              &rest initargs)
  (prog1
      (call-next-method)
    (dolist (protocol-name implements)
      (add-direct-protocol class (find-protocol protocol-name)))))

(defmethod add-direct-protocol ((class protocol-implementing-class) protocol)
  (pushnew protocol (slot-value class 'direct-protocols)))

(defmethod add-direct-protocol :after ((class protocol-implementing-class) protocol)
  "If the class is already finalized, we need to recompute the effective protocol list. If not, that
will happen at finalization time."
  (when (class-finalized-p class)
    (setf (slot-value class 'effective-protocols)
          (compute-effective-protocols class))))

(defmethod finalize-inheritance ((class protocol-implementing-class))
  (call-next-method)
  (setf (slot-value class 'effective-protocols)
        (compute-effective-protocols class)))
