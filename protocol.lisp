(in-package :protocols)

;;;;
;;;; The protocol Protocol
;;;; 
;;;; All protocol objects must be of a class that implements the protocol protocol.
;;;;

(defprotocol protocol ()
  (:documentation
   "All protocol metaobjects must be instances of a class that implements the protocol ~(protocol
protocol).

    Protocols exist primarily to be declared as being implemented by some class and to be referred
to in the specializer list of a signature. Classes which are declared to implement some protocol are
implicitly declared to implement the signatures which contain that protocol as a specializer.

    As protocols can be added to classes, signatures to generic-functions, and methods to
generic-functions at any time, there is no single instant in time at which the truth of the
previously mentioned declaration can be verified in a meaningful way. Therefore, the protocols
library simply provides three functions, ~(fun verify-class), ~(fun verify-protocol), and ~(fun
verify-generic-function) which verify, respectively, that a class, protocol, or generic-function
implements all of the signatures that it should.

    As part of the unit or regression tests for a system, those three functions should be used to
ensure that no parts of any protocols involved in that system are overlooked. The exact set of
classes, protocols, and generic-functions which should be verified is beyond the scope of this
documentation as it could become somewhat complex to determine if protocols are added to classes or
signatures to generic-functions that were defined as part of another system.

    Layering of protocols is considered a higher-order effect and is not addressed by this
system. (Re)initialization of objects is considered to be orthogonal to the use of those objects and
is not addressed by this system.

    The cannonical implementation of the protocol ~(protocol protocol) is the class ~(class
standard-protocol)."))
(defgeneric protocol-name (protocol)
  (:documentation
   "_Reader for protocol metaobjects_

    Returns the name of ~(arg protocol). This value can be any lisp object, but is usually a symbol
or ~(var nil) if the protocol has no name."))

(defsignature protocol-name (protocol))


(defgeneric (setf protocol-name) (new-name protocol)
  (:documentation
   "_Arguments_
    protocol is a protocol metaobject.
    new-name is any Lisp object.

    _Values_
    The value returned by this generic function is its ~(arg new-name) argument.

    _Purpose_

    This function changes the name of ~(arg protocol) to ~(arg new-name). This value is usually a
symbol, or ~(var nil) if the protocol has no name."))

(defsignature (setf protocol-name) (t protocol))


(defgeneric protocol-direct-superprotocols (protocol)
  (:documentation
   "_Reader for protocol metaobjects_

    Returns a list of the direct superprotocols of ~(arg protocol). The elements of this list are
protocol metaobjects. The empty list is returned if the protocol has no direct superprotocols."))

(defsignature protocol-direct-superprotocols (protocol))


(defgeneric protocol-precedence-list (protocol)
  (:documentation
   "_Reader for protocol metaobjects_

    Returns the protocol precedence list of the given protocol. The elements of this list are
protocol metaobjects.

    During protocol initialization shared-initialize calls compute-protocol-precedence-list to
compute the protocol precedence list of the protocol. That value is associated with the protocol
metaobject and is returned by protocol-precedence-list.

    _Note_

    There is no requirement for sorting the protocol precedence list in the same way that class
precedence lists must be sorted. This is acceptable because all inheritance of protocol
characteristics is merely additive. Specifically, there is no need to combine inherited attributes,
since the only inherited attribute is a signature, and the definition of a signature is the entirety
of what identifies it."))

(defsignature protocol-precedence-list (protocol))


(defgeneric compute-protocol-precedence-list (protocol)
  (:documentation
   "_Arguments_
    protocol is a protocol metaobject.

    _Values_
    The value returned by this generic function is a list of protocol metaobjects.

    _Purpose_

    This generic function is called to determine the protocol precedence list of a protocol.

    The result is a list which contains each of ~(arg protocol) and its superprotocols once and only
once. The ordering of the elements is unspecified for the reasons explained in the note in the
documentation for ~(fun protocol-precedence-list).

    All methods on this generic function must compute the protocol precedence list as a function of
the direct superprotocols of the superprotocols of ~(arg protocol). The results are undefined if the
rules used to compute the protocol precedence list depend on any other factors.

    When a protocol is initialized or reinitialized, ~(fun shared-initialize) calls this generic
function and associates the returned value with the protocol metaobject. This value can then be
accessed by calling ~(fun protocol-precedence-list).

    The list returned by this generic function will not be mutated by the implementation. The
results are undefined if a portable program mutates the list returned by this generic function."))

(defsignature compute-protocol-precedence-list (protocol))


(defgeneric protocol-direct-subprotocols (protocol)
  (:documentation
   "Reader for protocol metaobjects

    Returns a set of the direct subprotocols of ~(arg protocol). The elements of this set are
protocol metaobjects that all mention this class among their direct superprotocols. The empty set is
returned if ~(arg protocol) has no direct subprotocols. This value is maintained by the generic
functions ~(fun add-direct-subprotocol) and ~(fun remove-direct-subprotocol)."))

(defsignature protocol-direct-subprotocols (protocol))


(defgeneric add-direct-subprotocol (superprotocol subprotocol)
  (:documentation
   "_Arguments_
    superprotocol is a protocol metaobject.
    subprotocol is a protocol metaobject.

    _Values_
    The value returned by this generic function is unspecified.

    _Purpose_

    This generic function is called to maintain a set of backpointers from a protocol to its direct
subprotocols. It adds ~(arg subprotocol) to the set of direct subprotocols of ~(arg superprotocol).

    When a protocol is initialized, this generic function is called once for each direct
superprotocol of the protocol.

    When a protocol is reinitialized, this generic function is called once for each added direct
superprotocol of the class. The generic function ~(fun remove-direct-subprotocol) is called once for
each deleted direct superprotocol of the protocol.

    Methods of this generic function must take care that if they override an existing method, they
override ~(fun remove-direct-subprotocol) and ~(fun protocol-direct-subprotocols) accordingly."))

(defsignature add-direct-subprotocol (protocol protocol))


(defgeneric remove-direct-subprotocol (superprotocol subprotocol)
  (:documentation
   "_Arguments_
    superprotocol is a protocol metaobject.
    subprotocol is a protocol metaobject.

    _Values_
    The value returned by this generic function is unspecified.

    _Purpose_

    This generic function is called to maintain a set of backpointers from a protocol to its direct
subprotocols. It removes ~(arg subprotocol) from the set of direct subprotocols of ~(arg
superprotocol). No error is signalled if ~(arg subclass) is not in this set.

    When a protocol is reinitialized, this generic function is called once for each deleted direct
superprotocol of the protocol.

    Methods of this generic function must take care that if they override an existing method, they
override ~(fun add-direct-subprotocol) and ~(fun protocol-direct-subprotocols) accordingly."))

(defsignature remove-direct-subprotocol (protocol protocol))


(defsignature documentation (protocol (eql 't)))
(defsignature (setf documentation) (t protocol (eql 't)))
