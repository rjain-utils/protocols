(in-package :rjain.protocols)

;;;;
;;;; The class Standard-Protocol
;;;;
;;;; The cannonical implementation of the protocol Protocol is the class Standard-Protocol.
;;;;

(defclass standard-protocol ()
  ((name :initarg :name
         :reader protocol-name
         :initform nil)
   (direct-superprotocols :initarg :direct-superprotocols
                          :reader protocol-direct-superprotocols
                          :type list
                          :initform (list (find-protocol 't)))
   (precedence-list :reader protocol-precedence-list
                    :type list)
   (direct-subprotocols :reader protocol-direct-subprotocols
                        :type list
                        :initform '())
   (documentation :initarg :documentation
                  :type (or string null)
                  :initform nil))
  (:metaclass protocol-implementing-class)
  (implements protocol))

(defmethod shared-intialize :around ((protocol standard-protocol) slots &key)
  (let ((old-supers (protocol-direct-superprotocols protocol)))
    (call-next-method)
    (let* ((new-supers (protocol-direct-superprotocols protocol))
           (added-supers (set-difference new-supers old-supers))
           (removed-supers (set-difference old-supers new-supers)))
      ;; maintain the subprotocol backpointers
      (mapcar (lambda (superprotocol) (add-direct-subprotocol superprotocol protocol))
              added-supers)
      (mapcar (lambda (superprotocol) (remove-direct-subprotocol superprotocol protocol))
              removed-supers))
    (setf (slot-value protocol 'precedence-list)
          (compute-protocol-precedence-list protocol))
    (setf (find-protocol (protocol-name protocol))
          protocol))

(defmethod compute-protocol-precedence-list ((protocol standard-protocol))
  (cons protocol
        (apply #'nunion (mapcar #'protocol-precedence-list
                                (protocol-direct-superprotocols protocol)))))

(defmethod add-direct-subprotocol ((superprotocol standard-protocol) (subprotocol t))
  (push subprotocol (slot-value superprotocol 'direct-subprotocols)))

(defmethod remove-direct-subprotocol ((superprotocol standard-protocol) (subprotocol t))
  (deletef subprotocol (slot-value superprotocol 'direct-subprotocols)))

(defmethod documentation ((protocol standard-protocol) (type (eql 't)))
  (slot-value protocol 'documentation))

(defmethod (setf documentation) ((protocol standard-protocol) (type (eql 't)))
  (setf (slot-value protocol 'documentation)
        new-value))

